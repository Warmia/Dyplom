<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\TeamRepository")
 */
class Team
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $members;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $food;

    /**
     * @ORM\Column(type="integer")
     */
    private $cost;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $stay;



    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Concerts", mappedBy="team")
     */
    private $concerts;

    public function __construct()
    {
        $this->concerts = new ArrayCollection();
    }



    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName( $name)
    {
        $this->name = $name;

        return $this;
    }

    public function getMembers()
    {
        return $this->members;
    }

    public function setMembers( $members)
    {
        $this->members = $members;

        return $this;
    }

    public function getFood()
    {
        return $this->food;
    }

    public function setFood($food)
    {
        $this->food = $food;

        return $this;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost( $cost)
    {
        $this->cost = $cost;

        return $this;
    }

    public function getStay()
    {
        return $this->stay;
    }

    public function setStay( $stay)
    {
        $this->stay = $stay;

        return $this;
    }




    /**
     * @return Collection|Concerts[]
     */
    public function getConcerts(): Collection
    {
        return $this->concerts;
    }

    public function addConcert(Concerts $concert): self
    {
        if (!$this->concerts->contains($concert)) {
            $this->concerts[] = $concert;
            $concert->addTeam($this);
        }

        return $this;
    }

    public function removeConcert(Concerts $concert): self
    {
        if ($this->concerts->contains($concert)) {
            $this->concerts->removeElement($concert);
            $concert->removeTeam($this);
        }

        return $this;
    }



}
