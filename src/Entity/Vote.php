<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoteRepository")
 */
class Vote
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idTeam;

    /**
     * @ORM\Column(type="integer")
     */
    private $idUser;

    /**
     * @ORM\Column(type="integer")
     */
    private $idConcert;


    public function getId()
    {
        return $this->id;
    }

    public function getIdTeam()
    {
        return $this->idZespol;
    }

    public function setIdTeam(int $idTeam)
    {
        $this->idTeam = $idTeam;

        return $this;
    }

    public function getIdUser()
    {
        return $this->idUser;
    }

    public function setIdUser(int $idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }
    public function getIdConcert()
    {
        return $this->idConcert;
    }

    public function setIdConcert(int $idConcert)
    {
        $this->idConcert = $idConcert;

        return $this;
    }

}
