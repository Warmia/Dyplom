<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VotedCountRepository")
 */
class VotedCount
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_concert;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_team;

    /**
     * @ORM\Column(type="integer")
     */
    private $voted;

    public function __construct()
    {
        $this->voted = 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIdConcert()
    {
        return $this->id_concert;
    }

    public function setIdConcert(int $id_concert)
    {
        $this->id_concert = $id_concert;

        return $this;
    }

    public function getIdTeam()
    {
        return $this->id_team;
    }

    public function setIdTeam(int $id_team)
    {
        $this->id_team = $id_team;

        return $this;
    }

    public function getVoted()
    {
        return $this->voted;
    }

    public function setVoted(int $voted)
    {
        $this->voted = $voted;

        return $this;
    }
}
