<?php

namespace App\Controller;

use App\Entity\Concerts;
use App\Entity\Team;
use App\Form\ConcertsType;
use App\Form\TeamType;
use App\Repository\ConcertsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine;
/**
 * @Route("/organiser")
 */
class ConcertsController extends Controller
{
    /**
     * @Route("/", name="concerts_index", methods="GET")
     */
    public function index(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $concerts = $em->getRepository('App:Concerts')->findAll();


        $paginator  = $this->get('knp_paginator');

        $concert = $paginator->paginate(
            $concerts,
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 2)/*limit per page*/
        );
        return $this->render('concerts/index.html.twig', array(
            'concert' => $concert,
        ));


    }

    /**
     * @Route("/new", name="concerts_new", methods="GET|POST")
     */
    public function new(Request $request)
    {
        $concert = new Concerts();

        $form = $this->createForm(ConcertsType::class, $concert);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($concert);
            $em->flush();

            return $this->redirectToRoute('concerts_index');
        }

        return $this->render('concerts/new.html.twig', [
            'concert' => $concert,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/show/{id}", name="concerts_show", methods="GET")
     */
    public function show($id)
    {
        $em = $this->getDoctrine()->getManager();
        $concert = $em->getRepository('App:Concerts')->find($id);
        $team = $concert->getTeam();
        return $this->render('concerts/show.html.twig', ['concert' => $team,'id'=>$id]);
    }

    /**
     * @Route("/{id}/edit", name="concerts_edit", methods="GET|POST")
     */
    public function edit(Request $request, Concerts $concert)
    {
        $form = $this->createForm(ConcertsType::class, $concert);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('concerts_edit', ['id' => $concert->getId()]);
        }

        return $this->render('concerts/edit.html.twig', [
            'concert' => $concert,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="concerts_delete", methods="DELETE")
     */
    public function delete(Request $request, Concerts $concert)
    {
        if ($this->isCsrfTokenValid('delete'.$concert->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($concert);
            $em->flush();
        }

        return $this->redirectToRoute('concerts_index');
    }
}
