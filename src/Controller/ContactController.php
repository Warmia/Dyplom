<?php

namespace App\Controller;


use App\Form\ContactType;
use App\Repository\ConcertsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine;

class ContactController extends Controller
{
    /**
     * @Route("/contact",  name="contact")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(ContactType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contactFormData = $form->getData();

            foreach($contactFormData['receiver'] as $e) {

                $message = (new \Swift_Message('Organiser of concerts: ' . $contactFormData['title']))
                    ->setFrom('jan577628@gmail.com')
                    ->setTo($e->getEmail())
                    ->setBody($contactFormData['text'], 'text/plain');

                $mailer->send($message);
            }
            $this->addFlash(
                'notice',
                'You correct send e-mail!'
            );

        }
        return $this->render('contact\contact.html.twig', [
           'form' => $form->createView(),
        ]);
    }
}