<?php


namespace App\Controller;

use App\Entity\Concerts;
use App\Entity\Vote;
use App\Entity\VotedCount;
use App\Form\ConcertsType;
use App\Repository\ConcertsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * @Route("/participant")
 */
class ConcertsUserController extends Controller
{
    /**
     * @Route("/", name="concertsuser_index", methods="GET")
     */
    public function index(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $concerts = $em->getRepository('App:Concerts')->findAll();

        $paginator  = $this->get('knp_paginator');

        $concert = $paginator->paginate(
            $concerts,
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 2)/*limit per page*/
        );
        return $this->render('concerts/indexuser.html.twig', array(
            'concert' => $concert
        ));


    }


    /**
     * @Route("/{id}", name="concertsuser_show", methods="GET")
     */
    public function show($id)
    {
        $em = $this->getDoctrine()->getManager();
        $concert = $em->getRepository('App:Concerts')->find($id);
        $team = $concert->getTeam();
        return $this->render('concerts/showuser.html.twig', ['concert' => $team,'id'=>$id]);
    }

    /**
     * @Route("/{idConcert}/{id}", name="concertsuser_vote", methods="GET|POST")
     */
    public function vote( $idConcert ,$id , Request $request, UserInterface $user)
    {
        $em = $this->getDoctrine()->getManager();
        $userId = $user->getId();
        $votedCount  = $em->getRepository('App:VotedCount')->findOneBy(array('id_team' => $id,'id_concert' => $idConcert));
        if($votedCount == null) {
            $voteCount = new VotedCount();
            $voteCount -> setIdConcert($idConcert);
            $voteCount -> setIdTeam($id);
            $em->persist($voteCount);
            $em->flush();
        }

        $voted  = $em->getRepository('App:Vote')->findOneBy(array('idTeam' => $id,'idConcert' => $idConcert,'idUser' => $userId));
        if($voted == null) {

            $vote = new Vote();
            $vote->setIdTeam($id);
            $vote->setIdUser($userId);
            $vote->setIdConcert($idConcert);
            $teams = $em->getRepository('App:VotedCount')->findOneBy(array('id_team' => $id,'id_concert' => $idConcert));
            $team = $teams->getVoted();
            $team = $team + 1;
            $teams->setVoted($team);
            $em->persist($vote);
            $em->flush();
            $this->addFlash(
                'notice',
                'You voted!'
            );
        }
        else{
            $this->addFlash(
                'notice',
                'You can voted only once!'
            );
        }

        return $this->redirectToRoute('concertsuser_show',[ 'id'=> $idConcert]);


    }
    /**
     * @Route("/voted/{idConcert}/{id}", name="concertsuser_voted", methods="GET|POST")
     */
    public function  showVoted( $idConcert ,$id , Request $request, UserInterface $user)
    {
        $em = $this->getDoctrine()->getManager();
        $teams = $em->getRepository('App:VotedCount')->findOneBy(array('id_team' => $id,'id_concert' => $idConcert));
        if($teams == null)
        {
            $voting = 0;
        }
        else{
            $voting = $teams->getVoted();
        }
        $concert = $em->getRepository('App:Team')->find($id);
        $name = $concert->getName();

        return $this->render('concerts/vote.html.twig',['count'=> $voting,'id'=>$id, 'idConcert'=> $idConcert, 'name'=>$name]);

    }



}
