<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class MainController extends Controller
{
    /**
     * @Route("/main" , name="main")
     */
    public function mainAction()
    {


        return $this->render(
            'mainpage/main.html.twig'
        );
    }
    /**
     * @Route("/info" , name="info")
     */
    public function info(UserInterface $user)
    {
        $userRoles = $user->getRoles();
        $username = $user->getUsername();
        $email = $user->getEmail();
        return $this->render(
            'info/info.html.twig',
            [
                'role' => $userRoles,
                'username' => $username,
                'email' => $email
            ]
        );
    }
}

